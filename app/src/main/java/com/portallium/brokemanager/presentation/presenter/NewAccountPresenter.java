package com.portallium.brokemanager.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.portallium.brokemanager.presentation.view.NewAccountView;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
@InjectViewState
public class NewAccountPresenter extends MvpPresenter<NewAccountView> {

}
