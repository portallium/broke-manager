package com.portallium.brokemanager.presentation.presenter;


import android.util.Log;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.portallium.brokemanager.beans.account.Account;
import com.portallium.brokemanager.database.DatabaseManager;
import com.portallium.brokemanager.presentation.view.AccountsView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;

import java.util.List;

@InjectViewState
public class AccountsPresenter extends MvpPresenter<AccountsView> {

    private List<? extends Account> accounts;

    public void addAccount() {

    }

    public void getAccounts() {
        if (accounts != null) {
            getViewState().setAccounts(accounts);
        } else {
            DatabaseManager.INSTANCE.getAccounts()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<List<? extends Account>>() {
                        @Override
                        public void onSuccess(List<? extends Account> accounts) {
                            AccountsPresenter.this.accounts = accounts;
                            getViewState().setAccounts(accounts);
                            this.dispose();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("AccountsPresenter", e.getMessage(), e);
                            this.dispose();
                        }
                    });
        }
    }

}
