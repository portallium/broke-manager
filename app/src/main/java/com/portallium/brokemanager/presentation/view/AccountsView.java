package com.portallium.brokemanager.presentation.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.portallium.brokemanager.beans.account.Account;

import java.util.List;

public interface AccountsView extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setAccounts(List<? extends Account> accounts);
}
