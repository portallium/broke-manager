package com.portallium.brokemanager.database.resolvers.get;

import android.database.Cursor;
import android.support.annotation.NonNull;
import com.portallium.brokemanager.beans.Currency;
import com.portallium.brokemanager.beans.account.Account;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.operations.get.DefaultGetResolver;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public class AccountGetResolver<T extends Account> extends DefaultGetResolver<T> {

    private Account.AccountFactory<T> factory;

    public AccountGetResolver(Account.AccountFactory<T> factory) {
        this.factory = factory;
    }

    @NonNull
    @Override
    public T mapFromCursor(@NonNull StorIOSQLite storIOSQLite, @NonNull Cursor cursor) {
        return factory.instantiate(cursor.getInt(cursor.getColumnIndex(Account.DatabaseConstants.ID)),
                cursor.getString(cursor.getColumnIndex(Account.DatabaseConstants.NAME)),
                new BigDecimal(cursor.getString(cursor.getColumnIndex(Account.DatabaseConstants.AMOUNT))),
                Currency.fromCode(cursor.getInt(cursor.getColumnIndex(Account.DatabaseConstants.CURRENCY))));
    }


}
