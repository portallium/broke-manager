package com.portallium.brokemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.portallium.brokemanager.R;
import com.portallium.brokemanager.beans.Currency;
import com.portallium.brokemanager.beans.account.*;
import com.portallium.brokemanager.database.resolvers.delete.AccountDeleteResolver;
import com.portallium.brokemanager.database.resolvers.get.AccountGetResolver;
import com.portallium.brokemanager.database.resolvers.put.AccountPutResolver;
import com.pushtorefresh.storio3.sqlite.SQLiteTypeMapping;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "bm.db";
    private static final int DATABASE_VERSION = 1;

    private static String DEFAULT_ACCOUNT_CASH;
    private static String DEFAULT_ACCOUNT_BANK_CARD;

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        DEFAULT_ACCOUNT_CASH = context.getString(R.string.cash_account);
        DEFAULT_ACCOUNT_BANK_CARD = context.getString(R.string.bank_card_account);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BonusCardAccount.DatabaseConstants.createTableQuery(BonusCardAccount.class));
        db.execSQL(CreditCardAccount.DatabaseConstants.createTableQuery(CreditCardAccount.class));
        db.execSQL(DebitCardAccount.DatabaseConstants.createTableQuery(DebitCardAccount.class));
        db.execSQL(CashAccount.DatabaseConstants.createTableQuery(CashAccount.class));
        db.execSQL(GiftCardAccount.DatabaseConstants.createTableQuery(GiftCardAccount.class));
        db.execSQL(TransportCardAccount.DatabaseConstants.createTableQuery(TransportCardAccount.class));

        ContentValues cv = new ContentValues();
        cv.put(CashAccount.DatabaseConstants.AMOUNT, "0");
        cv.put(CashAccount.DatabaseConstants.NAME, DEFAULT_ACCOUNT_CASH);
        cv.put(CashAccount.DatabaseConstants.CURRENCY, Currency.RUB.getCode()); // TODO: 28.10.2018 а если иностранец!
        db.insert(CashAccount.class.getSimpleName(), null, cv);
        cv.put(DebitCardAccount.DatabaseConstants.NAME, DEFAULT_ACCOUNT_BANK_CARD);
        db.insert(DebitCardAccount.class.getSimpleName(), null, cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
