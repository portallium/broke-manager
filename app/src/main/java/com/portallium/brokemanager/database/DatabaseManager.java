package com.portallium.brokemanager.database;

import android.content.Context;
import com.portallium.brokemanager.beans.account.*;
import com.portallium.brokemanager.database.resolvers.delete.AccountDeleteResolver;
import com.portallium.brokemanager.database.resolvers.get.AccountGetResolver;
import com.portallium.brokemanager.database.resolvers.put.AccountPutResolver;
import com.pushtorefresh.storio3.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.impl.DefaultStorIOSQLite;
import com.pushtorefresh.storio3.sqlite.queries.Query;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public enum DatabaseManager {
    INSTANCE;

    private StorIOSQLite storio;

    public void init(Context context) {
        storio = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(new DatabaseHelper(context))
                .defaultRxScheduler(Schedulers.io())
                .addTypeMapping(CashAccount.class, new AccountSQLTypeMapping<>(new CashAccount.CashAccountFactory()))
                .addTypeMapping(BonusCardAccount.class, new AccountSQLTypeMapping<>(new BonusCardAccount.BonusCardAccountFactory()))
                .addTypeMapping(CreditCardAccount.class, new AccountSQLTypeMapping<>(new CreditCardAccount.CreditCardAccountFactory()))
                .addTypeMapping(DebitCardAccount.class, new AccountSQLTypeMapping<>(new DebitCardAccount.DebitCardAccountFactory()))
                .addTypeMapping(GiftCardAccount.class, new AccountSQLTypeMapping<>(new GiftCardAccount.GiftCardAccountFactory()))
                .addTypeMapping(TransportCardAccount.class, new AccountSQLTypeMapping<>(new TransportCardAccount.TransportCardAccountFactory()))
                .build();
    }

    public Single<List<Account>> getAccounts() {
        return Single.fromCallable(() -> {
            List<Account> accounts = new ArrayList<>();
            accounts.addAll(getObjectList(BonusCardAccount.class));
            accounts.addAll(getObjectList(CashAccount.class));
            accounts.addAll(getObjectList(CreditCardAccount.class));
            accounts.addAll(getObjectList(DebitCardAccount.class));
            accounts.addAll(getObjectList(GiftCardAccount.class));
            accounts.addAll(getObjectList(TransportCardAccount.class));
            return accounts;
        }).subscribeOn(Schedulers.io());
    }

    private <T> List<? extends T> getObjectList(Class<? extends T> t) {
        return storio.get()
                .listOfObjects(t)
                .withQuery(Query.builder().table(t.getSimpleName()).build())
                .prepare()
                .executeAsBlocking();
    }

    private static class AccountSQLTypeMapping<T extends Account> extends SQLiteTypeMapping<T> {
        AccountSQLTypeMapping(Account.AccountFactory<T> factory) {
            super(new AccountPutResolver<>(), new AccountGetResolver<>(factory), new AccountDeleteResolver<>());
        }
    }
}
