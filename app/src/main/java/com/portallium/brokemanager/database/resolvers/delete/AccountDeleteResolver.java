package com.portallium.brokemanager.database.resolvers.delete;

import android.support.annotation.NonNull;
import com.portallium.brokemanager.beans.account.Account;
import com.pushtorefresh.storio3.sqlite.operations.delete.DefaultDeleteResolver;
import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public class AccountDeleteResolver<T extends Account> extends DefaultDeleteResolver<T> {
    @NonNull
    @Override
    protected DeleteQuery mapToDeleteQuery(@NonNull T object) {
        return DeleteQuery.builder()
                .table(object.getClass().getSimpleName())
                .where(T.DatabaseConstants.ID + " = ?")
                .whereArgs(object.getId())
                .build();
    }
}
