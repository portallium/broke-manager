package com.portallium.brokemanager.database.resolvers.put;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import com.portallium.brokemanager.beans.account.Account;
import com.pushtorefresh.storio3.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio3.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio3.sqlite.queries.UpdateQuery;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public class AccountPutResolver<T extends Account> extends DefaultPutResolver<T> {
    @NonNull
    @Override
    protected InsertQuery mapToInsertQuery(@NonNull T object) {
        return InsertQuery.builder()
                .table(object.getClass().getSimpleName())
                .build();
    }

    @NonNull
    @Override
    protected UpdateQuery mapToUpdateQuery(@NonNull T object) {
        return UpdateQuery.builder()
                .table(object.getClass().getSimpleName())
                .where(Account.DatabaseConstants.ID + " = ?")
                .whereArgs(object.getId())
                .build();
    }

    @NonNull
    @Override
    protected ContentValues mapToContentValues(@NonNull T object) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Account.DatabaseConstants.NAME, object.getName());
        contentValues.put(Account.DatabaseConstants.AMOUNT, object.getAmount().toString());
        contentValues.put(Account.DatabaseConstants.CURRENCY, object.getCurrency().getCode());
        return contentValues;
    }
}
