package com.portallium.brokemanager.beans;

import org.jetbrains.annotations.Contract;

/**
 * Created by Vladimir Sechkarev on 28.10.2018.
 */
public enum Currency {

    RUB(643),
    EUR(978),
    USD(840);

    private final int code;

    Currency(final int code) {
        this.code = code;
    }

    @Contract(pure = true)
    public int getCode() {
        return code;
    }

    public static Currency fromCode(int code) {
        switch (code) {
            case 643:
                return RUB;
            case 978:
                return EUR;
            case 840:
                return USD;
            default:
                throw new IllegalArgumentException(code + " is an unknown currency code");
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case RUB:
                return "\u20BD";
            case USD:
                return "\u0024";
            case EUR:
                return "\u20ac";
        }
        return super.toString();
    }
}
