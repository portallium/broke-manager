package com.portallium.brokemanager.beans;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public class MoneyTransaction {
    private final int id;
    private final BigDecimal amount;
    private final Date timestamp;
    private final int fromAccount;
    private final Integer toAccount; //can be null

    public MoneyTransaction(int id, BigDecimal amount, Date timestamp, int fromAccount) {
        this(id, amount, timestamp, fromAccount, null);
    }

    private MoneyTransaction(int id, BigDecimal amount, Date timestamp, int fromAccount, Integer toAccount) {
        this.id = id;
        this.amount = amount;
        this.timestamp = timestamp;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }
}
