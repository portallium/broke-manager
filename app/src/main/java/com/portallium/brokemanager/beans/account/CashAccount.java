package com.portallium.brokemanager.beans.account;

import com.portallium.brokemanager.beans.Currency;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class CashAccount extends Account {

    public CashAccount(int id, String name, BigDecimal amount, Currency currency) {
        super(id, name, amount, currency);
    }

    public static class CashAccountFactory implements AccountFactory<CashAccount> {
        @Override
        public CashAccount instantiate(int id, String name, BigDecimal amount, Currency currency) {
            return new CashAccount(id, name, amount, currency);
        }
    }
}
