package com.portallium.brokemanager.beans.account;

import com.portallium.brokemanager.beans.Currency;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class BonusCardAccount extends Account {

    public BonusCardAccount(int id, String name, BigDecimal amount, Currency currency) {
        super(id, name, amount, currency);
    }

    public static class BonusCardAccountFactory implements AccountFactory<BonusCardAccount> {
        @Override
        public BonusCardAccount instantiate(int id, String name, BigDecimal amount, Currency currency) {
            return new BonusCardAccount(id, name, amount, currency);
        }
    }

}
