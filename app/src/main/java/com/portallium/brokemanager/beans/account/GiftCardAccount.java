package com.portallium.brokemanager.beans.account;

import com.portallium.brokemanager.beans.Currency;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class GiftCardAccount extends Account {
    public GiftCardAccount(int id, String name, BigDecimal amount, Currency currency) {
        super(id, name, amount, currency);
    }

    public static class GiftCardAccountFactory implements AccountFactory<GiftCardAccount> {
        @Override
        public GiftCardAccount instantiate(int id, String name, BigDecimal amount, Currency currency) {
            return new GiftCardAccount(id, name, amount, currency);
        }
    }
}
