package com.portallium.brokemanager.beans.account;

import android.support.annotation.NonNull;
import com.portallium.brokemanager.beans.Currency;
import org.jetbrains.annotations.Contract;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public abstract class Account {

    public interface AccountFactory<T extends Account> {
        T instantiate(int id, String name, BigDecimal amount, Currency currency);
    }

    private final int id;
    private String name;
    private BigDecimal amount;
    private Currency currency;

    public Account(int id, String name, BigDecimal amount, Currency currency) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.currency = currency;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                '}';
    }

    public static class DatabaseConstants {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String AMOUNT = "amount";
        public static final String CURRENCY = "currency";

        @Contract(pure = true)
        @NonNull
        public static String createTableQuery(@NonNull Class<? extends Account> accountClass) {
            return "CREATE TABLE "
                    + accountClass.getSimpleName()
                    + "("
                    + ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + NAME
                    + " TEXT NOT NULL, "
                    + AMOUNT
                    + " TEXT NOT NULL, "
                    + CURRENCY
                    + " INTEGER NOT NULL"
                    + ");";
        }
    }
}
