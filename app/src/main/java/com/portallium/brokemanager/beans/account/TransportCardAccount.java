package com.portallium.brokemanager.beans.account;

import com.portallium.brokemanager.beans.Currency;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class TransportCardAccount extends Account {

    public TransportCardAccount(int id, String name, BigDecimal amount, Currency currency) {
        super(id, name, amount, currency);
    }

    public static class TransportCardAccountFactory implements AccountFactory<TransportCardAccount> {
        @Override
        public TransportCardAccount instantiate(int id, String name, BigDecimal amount, Currency currency) {
            return new TransportCardAccount(id, name, amount, currency);
        }
    }
}
