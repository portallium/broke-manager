package com.portallium.brokemanager.beans.account;

import com.portallium.brokemanager.beans.Currency;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class DebitCardAccount extends Account {
    public DebitCardAccount(int id, String name, BigDecimal amount, Currency currency) {
        super(id, name, amount, currency);
    }

    public static class DebitCardAccountFactory implements AccountFactory<DebitCardAccount> {
        @Override
        public DebitCardAccount instantiate(int id, String name, BigDecimal amount, Currency currency) {
            return new DebitCardAccount(id, name, amount, currency);
        }
    }
}
