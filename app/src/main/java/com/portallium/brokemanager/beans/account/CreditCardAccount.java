package com.portallium.brokemanager.beans.account;

import com.portallium.brokemanager.beans.Currency;

import java.math.BigDecimal;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class CreditCardAccount extends Account {
    public CreditCardAccount(int id, String name, BigDecimal amount, Currency currency) {
        super(id, name, amount, currency);
    }

    public static class CreditCardAccountFactory implements AccountFactory<CreditCardAccount> {
        @Override
        public CreditCardAccount instantiate(int id, String name, BigDecimal amount, Currency currency) {
            return new CreditCardAccount(id, name, amount, currency);
        }
    }
}
