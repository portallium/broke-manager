package com.portallium.brokemanager.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.portallium.brokemanager.R;
import com.portallium.brokemanager.ui.fragment.AccountsFragment;

public class MoxyActivity extends MvpAppCompatActivity /*implements MoxyView*/ {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moxy);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new DrawerBuilder(this)
                .withToolbar(toolbar)
                .withTranslucentStatusBar(true)
                .addDrawerItems(new PrimaryDrawerItem().withName(R.string.accounts))
                .build();

        if (savedInstanceState == null) { //иначе фрагменты (и презентеры) будут пересоздаваться по новой
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.core_content_frame, AccountsFragment.newInstance())
                    .commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("test", "test");
    }
}
