package com.portallium.brokemanager.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.portallium.brokemanager.R;
import com.portallium.brokemanager.beans.account.*;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class AccountsAdapter extends RecyclerView.Adapter {

    private List<? extends Account> accounts;

    public AccountsAdapter(Context context, List<? extends Account> accounts) {
        this.accounts = accounts;
        AccountsViewHolderData.fillData(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AccountsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_account, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Account account = accounts.get(position);
        ((AccountsViewHolder) holder).bind(account);
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    private class AccountsViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView balance;

        private AccountsViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.account_type_image);
            name = itemView.findViewById(R.id.account_name);
            balance = itemView.findViewById(R.id.account_balance);
        }

        private void bind(Account account) {
            Drawable imageDrawable = null;
            if (account instanceof CashAccount) {
                imageDrawable = AccountsViewHolderData.walletDrawable;
            } else if (account instanceof CreditCardAccount) {
                imageDrawable = AccountsViewHolderData.creditCardDrawable;
            } else if (account instanceof TransportCardAccount) {
                imageDrawable = AccountsViewHolderData.transportCardDrawable;
            } else if (account instanceof GiftCardAccount) {
                imageDrawable = AccountsViewHolderData.giftCardDrawable;
            } else if (account instanceof BonusCardAccount) {
                imageDrawable = AccountsViewHolderData.bonusCardDrawable;
            } else if (account instanceof DebitCardAccount) {
                imageDrawable = AccountsViewHolderData.debitCardDrawable;
            }
            image.setImageDrawable(imageDrawable);
            name.setText(account.getName());
            balance.setText(String.format("%s %s", account.getAmount().toPlainString(), account.getCurrency().toString()));
            balance.setTextColor(account.getAmount().compareTo(new BigDecimal(0)) < 0 ? AccountsViewHolderData.redTextColor : AccountsViewHolderData.greenTextColor);
        }
    }

    private static class AccountsViewHolderData {
        private static Drawable walletDrawable;
        private static Drawable creditCardDrawable;
        private static Drawable debitCardDrawable;
        private static Drawable transportCardDrawable;
        private static Drawable giftCardDrawable;
        private static Drawable bonusCardDrawable;
        private static @ColorInt int redTextColor;
        private static @ColorInt int greenTextColor;

        private static void fillData(@NotNull Context context) {
            walletDrawable = context.getResources().getDrawable(R.drawable.ic_baseline_wallet_24px);
            creditCardDrawable = context.getResources().getDrawable(R.drawable.ic_baseline_credit_card_24px);
            debitCardDrawable = context.getResources().getDrawable(R.drawable.ic_baseline_debit_card_24px);
            transportCardDrawable = context.getResources().getDrawable(R.drawable.ic_baseline_transport_card_24px);
            giftCardDrawable = context.getResources().getDrawable(R.drawable.ic_baseline_card_giftcard_24px);
            bonusCardDrawable = context.getResources().getDrawable(R.drawable.ic_baseline_bonus_card_24px);
            redTextColor = context.getResources().getColor(R.color.redTextColor);
            greenTextColor = context.getResources().getColor(R.color.greenTextColor);
        }
    }
}
