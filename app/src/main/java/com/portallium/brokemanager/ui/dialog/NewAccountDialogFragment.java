package com.portallium.brokemanager.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.portallium.brokemanager.R;
import com.portallium.brokemanager.presentation.presenter.NewAccountPresenter;
import com.portallium.brokemanager.presentation.view.NewAccountView;

/**
 * Created by Vladimir Sechkarev on 27.10.2018.
 */
public class NewAccountDialogFragment extends MvpAppCompatDialogFragment implements NewAccountView {

    @InjectPresenter
    NewAccountPresenter presenter;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View v = View.inflate(getActivity(), R.layout.dialog_new_account, null);

        AppCompatSpinner accountTypeSpinner = v.findViewById(R.id.new_account_spinner);
        accountTypeSpinner.setAdapter(ArrayAdapter.createFromResource(requireContext(), R.array.account_types, android.R.layout.simple_spinner_dropdown_item));

        return new AlertDialog.Builder(requireContext())
                .setTitle(R.string.new_account_dialog_title)
                .setView(v)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }
}
