package com.portallium.brokemanager.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.portallium.brokemanager.R;
import com.portallium.brokemanager.beans.account.Account;
import com.portallium.brokemanager.presentation.presenter.AccountsPresenter;
import com.portallium.brokemanager.presentation.view.AccountsView;
import com.portallium.brokemanager.ui.adapter.AccountsAdapter;
import com.portallium.brokemanager.ui.dialog.NewAccountDialogFragment;

import java.util.List;

public class AccountsFragment extends MvpAppCompatFragment implements AccountsView {

    @InjectPresenter
    AccountsPresenter mAccountsPresenter;

    RecyclerView recyclerView;

    public static AccountsFragment newInstance() {
        AccountsFragment fragment = new AccountsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_accounts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.accounts_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(requireContext(), linearLayoutManager.getOrientation()));
        mAccountsPresenter.getAccounts();

        view.findViewById(R.id.accounts_fab).setOnClickListener(v ->
                new NewAccountDialogFragment().show(requireFragmentManager(), "new dialog")
        );
    }

    @Override
    public void setAccounts(List<? extends Account> accounts) {
        recyclerView.setAdapter(new AccountsAdapter(getActivity(), accounts));
    }
}
