package com.portallium.brokemanager;

import android.app.Application;
import com.portallium.brokemanager.database.DatabaseManager;

/**
 * Created by Vladimir Sechkarev on 21.10.2018.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseManager.INSTANCE.init(this);
    }
}
